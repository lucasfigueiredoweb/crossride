/**
 * 
 */
package com.crossover.techtrial.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Ride;

/**
 * @author crossover
 *
 */
@RestResource(exported = false)
public interface RideRepository extends CrudRepository<Ride, Long> {

	@Query(value = "select p.*,  top5.duration_seconds / 1 as totalRideDurationInSeconds, top5.max_seconds / 1 as maxRideDurationInSeconds, "
			+ "top5.avg_distance as averageDistance , top5.driver_id as driver_id,top5.rider_id as rider_id, top5.start_time as start_time, top5.end_time as end_time, top5.distance as distance "
			+ "from (select r.driver_id as driver_id, r.start_time as start_time, r.end_time as end_time, r.distance as distance, r.rider_id as rider_id, "
			+ "avg(r.distance) as avg_distance, "
			+ "sum(to_seconds(r.end_time) - to_seconds(r.start_time)) as duration_seconds, "
			+ "max(to_seconds(r.end_time) - to_seconds(r.start_time)) as max_seconds " + "from ride r "
			+ "where r.start_time >= '2018-08-08T12:12:12'  and " + "r.end_time <= '2018-08-08T18:12:12' "
			+ "group by r.driver_id " + "order by duration_seconds desc " + "limit 5 " + ") top5 join " + "person p "
			+ "on top5.driver_id = p.id", nativeQuery = true)
	ArrayList<TopDriverDTO> getFiveRidersWithDuration();

}
