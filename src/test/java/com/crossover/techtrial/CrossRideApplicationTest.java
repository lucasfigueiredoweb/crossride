/**
 * 
 */
package com.crossover.techtrial;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.crossover.techtrial.controller.RideController;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.RideRepository;

/**
 * @author Lucas Figueiredo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CrossRideApplicationTest {

	MockMvc mockMvc;

	@Mock
	private RideController riderController;

	@Autowired
	private TestRestTemplate template;

	@Autowired
	RideRepository riderRepository;

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(riderController).build();
	}

	@Test
	public void testRegisterRider() throws Exception {
		Ride rider = new Ride();
		
		HttpEntity<Object> personDriver = getHttpEntity(
		        "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\"," 
		            + " \"registrationNumber\": \"41DCT\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
		    ResponseEntity<Person> responseDriver = template.postForEntity(
		        "/api/person", personDriver, Person.class);
		rider.setDriver(responseDriver.getBody());
		rider.setStartTime("2018-08-08T12:20:20");
		rider.setEndTime("2018-08-08T12:30:30");
		rider.setDistance(1000L);
		
		HttpEntity<Object> personRider = getHttpEntity(
		        "{\"name\": \"test 2\", \"email\": \"test10000000000002@gmail.com\"," 
		            + " \"registrationNumber\": \"42DCT\",\"registrationDate\":\"2018-08-08T12:13:13\" }");
		    ResponseEntity<Person> responseRider = template.postForEntity(
		        "/api/person", personRider, Person.class);
		    
		rider.setRider(responseRider.getBody());
		

		    ResponseEntity<Ride> responseNewRider = template.postForEntity(
		        "/api/ride", rider, Ride.class);
		
		Assert.assertNotNull(responseNewRider.getBody());
		Assert.assertEquals("test 1", responseDriver.getBody().getName());
		Assert.assertEquals("test 2", responseRider.getBody().getName());
		Assert.assertNotEquals(responseDriver.getBody(), responseRider.getBody());
		Assert.assertFalse(responseDriver.equals(responseRider));
		Assert.assertEquals(200, responseNewRider.getStatusCode().value());
		Assert.assertEquals(200, responseDriver.getStatusCode().value());
		Assert.assertEquals(200, responseRider.getStatusCode().value());
	}
	
	@Test
	public void testToString() throws Exception {
		Ride rider = new Ride();
		
		HttpEntity<Object> personDriver = getHttpEntity(
		        "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\"," 
		            + " \"registrationNumber\": \"41DCT\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
		    ResponseEntity<Person> responseDriver = template.postForEntity(
		        "/api/person", personDriver, Person.class);
		rider.setDriver(responseDriver.getBody());
		rider.setStartTime("2018-08-08T12:20:20");
		rider.setEndTime("2018-08-08T12:30:30");
		rider.setDistance(1000L);
		
		HttpEntity<Object> personRider = getHttpEntity(
		        "{\"name\": \"test 2\", \"email\": \"test10000000000002@gmail.com\"," 
		            + " \"registrationNumber\": \"42DCT\",\"registrationDate\":\"2018-08-08T12:13:13\" }");
		    ResponseEntity<Person> responseRider = template.postForEntity(
		        "/api/person", personRider, Person.class);
		rider.setRider(responseRider.getBody());
		
		 ResponseEntity<Ride> responseNewRider = template.postForEntity(
			        "/api/ride", rider, Ride.class);
			
			Assert.assertNotNull(responseNewRider.getBody().toString());
			Assert.assertEquals("test 1", responseDriver.getBody().getName());
			Assert.assertEquals("test 2", responseRider.getBody().getName());
			Assert.assertNotEquals(responseDriver.getBody(), responseRider.getBody());
			Assert.assertFalse(responseDriver.equals(responseRider));
			Assert.assertEquals(200, responseNewRider.getStatusCode().value());
			Assert.assertEquals(200, responseDriver.getStatusCode().value());
			Assert.assertEquals(200, responseRider.getStatusCode().value());
	}
	
	@Test
	public void testHashCode() throws Exception {
		Ride rider = new Ride();
		
		HttpEntity<Object> personDriver = getHttpEntity(
		        "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\"," 
		            + " \"registrationNumber\": \"41DCT\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
		    ResponseEntity<Person> responseDriver = template.postForEntity(
		        "/api/person", personDriver, Person.class);
		rider.setDriver(responseDriver.getBody());
		rider.setStartTime("2018-08-08T12:20:20");
		rider.setEndTime("2018-08-08T12:30:30");
		rider.setDistance(1000L);
		
		HttpEntity<Object> personRider = getHttpEntity(
		        "{\"name\": \"test 2\", \"email\": \"test10000000000002@gmail.com\"," 
		            + " \"registrationNumber\": \"42DCT\",\"registrationDate\":\"2018-08-08T12:13:13\" }");
		    ResponseEntity<Person> responseRider = template.postForEntity(
		        "/api/person", personRider, Person.class);
		    
		    ResponseEntity<Ride> responseNewRider = template.postForEntity(
			        "/api/ride", rider, Ride.class);
			
			Assert.assertNotNull(responseNewRider.getBody().hashCode());
			Assert.assertEquals("test 1", responseDriver.getBody().getName());
			Assert.assertEquals("test 2", responseRider.getBody().getName());
			Assert.assertNotEquals(responseDriver.getBody(), responseRider.getBody());
			Assert.assertFalse(responseDriver.equals(responseRider));
			Assert.assertEquals(200, responseNewRider.getStatusCode().value());
			Assert.assertEquals(200, responseDriver.getStatusCode().value());
			Assert.assertEquals(200, responseRider.getStatusCode().value());
	}

	private HttpEntity<Object> getHttpEntity(Object body) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<Object>(body, headers);
	}

}
